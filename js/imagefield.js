
var iDrupal = {
  // Attach an image file back to a node form.
  attachImage: function(fid, id) {
    $('#' + id).val(fid);
  },
  
  // Let the client know it needs to pull up the imagePicker.
  imagePicker: function(id) {
    document.location = "idrupal://imagePicker/"+ id;
  },
  
  init: function() {
    // Replace each imagefield with what we need to handle the cameraPicker on the iphone.
    $('.idrupal-imagefield').each(function() {
      $('input[@type="file"]', this).each(function() {
        // Hide the field that is completely useless to us off the screen.
        $(this).css({position: 'absolute', top: '-1000%'});
        //var label = $($('label', this)[0]).html();
        
        var id = this.id.replace('upload', 'fid');
      
        // TODO: replace with a theme function and pass through via Drupal.settings.
        $('<input class="image-picker" type="button" value="Select" />')
        .click(function(event) { iDrupal.imagePicker(id); return false; })
        .insertAfter(this);
      });
    });
  }  
};

$(document).ready(iDrupal.init);
